#include "FreeTetrominoes.hpp"

std::string read(std::string);
bool inRect(sf::Vector2i p, sf::Vector2i q, sf::Vector2i s) {
  return p.x >= q.x && p.x < q.x + s.x
      && p.y >= q.y && p.y < q.y + s.y;
}

bool FreeTetrominoes::hasTet(const MaybeTet &m) const {
  return m != tetrominoes.end();
}

FreeTetrominoes::FreeTetrominoes(int size):
  size(size) {
  std::string path("res/tetrominoes/");
  int x = 780;
  for (int i = 0; i < 2; ++i) {
    int y = 20;
    for (auto &tet : {"knight", "line", "snake", "square", "tee"}) {
      tetrominoes.push_back({{x, y}, Tetromino::fromString(read(path + tet))});
      tetrominoes.back().tet.flipOnV(); // DON'T TOUCH! THIS FIXES STUFF!
                                        // I DON'T KNOW WHY
      y += 120;
    }
    x += 260;
  }
}

MaybeTet FreeTetrominoes::put(Tetromino t, sf::Vector2i p) {
  tetrominoes.push_back({p, t});
  return --tetrominoes.end();
}

MaybeTet FreeTetrominoes::get(sf::Vector2i pos) {
  auto iter = tetrominoes.begin();
  auto end = tetrominoes.end();
  while (iter != end) {
    if (inRect(pos, iter->pos
              ,{iter->tet.getWidth() * size, iter->tet.getHeight() * size}
    )) {
      return iter;
    }
    ++iter;
  }
  return end;
}

void FreeTetrominoes::remove(MaybeTet m) {
  tetrominoes.erase(m);
}

MaybeTet FreeTetrominoes::empty() {
  return tetrominoes.end();
}

void FreeTetrominoes::render(sf::RenderWindow &w) const {
  for (auto &pair : tetrominoes) {
    pair.tet.renderAt(pair.pos, size, w);
  }
}
