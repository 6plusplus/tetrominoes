#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "FreeTetrominoes.hpp"
#include "Board.hpp"
#include <string>
#include <sstream>
#include <fstream>

#include <cmath>

const int width = 1300, height = 700; // Should be non-const
                                      // The window stretches when resized

const int size = 50;

std::string read(std::string);

int main(int argc, const char **argv) {
  int clickCount = 0;
  float timeTaken = 0;

  sf::Font font;
  font.loadFromFile("/System/Library/Fonts/Helvetica.dfont");

  sf::RenderWindow window(sf::VideoMode(width, height), "Tetrominoes");
  sf::Event event;

  auto board = Board::fromString(read(argv[1]));
  FreeTetrominoes freeTetrominoes(size);
  auto held = freeTetrominoes.empty();

  int bpx = (720 - (size * board.getWidth())) / 2;
  int bpy = (height - (size * board.getHeight())) / 2;

  int mX = sf::Mouse::getPosition(window).x;
  int mY = sf::Mouse::getPosition(window).y;

  bool running = true;

  while (running) {
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::MouseButtonPressed) {
        running = false;
      }
    }
    window.clear(sf::Color::White);
    window.display();
  }

  sf::Clock clock;

  running = true;

  while (running) {
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) {
        window.close();
        running = false;
      } else
      if (freeTetrominoes.hasTet(held) && event.type == sf::Event::KeyPressed) {
        switch (event.key.code) {
          case sf::Keyboard::W:
            held->tet.flipOnH();
            break;
          case sf::Keyboard::F:
            held->tet.flipOnV();
            break;
          case sf::Keyboard::A:
            held->tet.rotateAC();
            break;
          case sf::Keyboard::D:
            held->tet.rotateC();
            break;
          default:;
        }
      } else
      if (event.type == sf::Event::MouseMoved) {
        if (freeTetrominoes.hasTet(held)) {
          held->pos.x += event.mouseMove.x - mX;
          held->pos.y += event.mouseMove.y - mY;
        }
        mX = event.mouseMove.x;
        mY = event.mouseMove.y;
      } else
      if (event.type == sf::Event::MouseButtonPressed) {
        held = freeTetrominoes.get({event.mouseButton.x, event.mouseButton.y});
        if (!freeTetrominoes.hasTet(held)) {
          Tetromino tempT;
          Position tempP = {(mX - bpx)/size, (mY - bpy)/size};
          if (board.tryTake(tempT, tempP)) {
            held = freeTetrominoes.put(tempT, size * sf::Vector2i{tempP.x, tempP.y} + sf::Vector2i{bpx, bpy});
            if (freeTetrominoes.hasTet(held)) {
              held->tet.color = sf::Color(255, 127, 0);
            }
          }
        }
        ++clickCount;
      } else
      if (freeTetrominoes.hasTet(held)
       && event.type == sf::Event::MouseButtonReleased)
      {
        int posX = round((held->pos.x - bpx) / size);
        int posY = round((held->pos.y - bpy) / size);
        held->tet.color = sf::Color(255, 200, 0);
        if (board.tryAdd(held->tet, {posX, posY})) {
          freeTetrominoes.remove(held);
        } else {
          held->tet.color = sf::Color(255, 127, 0);
        }
        held = freeTetrominoes.empty();
        if (board.isFinished()) {
          running = false;
          timeTaken = clock.getElapsedTime().asSeconds();
        }
      }
    }

    window.clear(sf::Color::White);
    board.renderAt({bpx, bpy}, size, window);
    freeTetrominoes.render(window);
    window.display();

    if (clock.getElapsedTime().asSeconds() > (3 * 60)) {
      window.close();
      running = false;
    }
  }

  board.onFinish();

  sf::Text clickText{"Number of clicks: " + std::to_string(clickCount), font, 30};
  clickText.setPosition(10, 10);
  clickText.setFillColor(sf::Color::Black);

  // TODO: This needs to round to 2dp
  sf::Text timeText{"Time taken: " + std::to_string(timeTaken) + "s", font, 30};
  timeText.setPosition(10, 50);
  timeText.setFillColor(sf::Color::Black);

  bool clicked = false;

  while (window.isOpen()) {
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) {
        window.close();
        running = false;
      } else
      if (event.type == sf::Event::MouseButtonPressed) {
        clicked = true;
      }
    }
    window.clear(sf::Color::White);
    if (clicked) {
      window.draw(clickText);
      window.draw(timeText);
    } else {
      board.renderAt({bpx, bpy}, size, window);
      freeTetrominoes.render(window);
    }
    window.display();
  }

  return 0;
}

std::string read(std::string path) {
  std::ifstream file(path);
  std::stringstream sstr;
  sstr << file.rdbuf();
  return sstr.str();
}
