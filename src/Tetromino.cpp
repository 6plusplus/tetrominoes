#include "Tetromino.hpp"

template<typename T>
void swap(T &x, T &y) {
  T temp = x;
  x = y;
  y = temp;
}

void Tetromino::rotateAC() {
  for (auto &cell : cells) {
    cell = {cell.y, width - cell.x - 1};
  }
  swap(width, height);
}

void Tetromino::rotateC() {
  for (auto &cell : cells) {
    cell = {height - cell.y - 1, cell.x};
  }
  swap(width, height);
}

void Tetromino::flipOnH() {
  for (auto &cell : cells) {
    cell = {cell.x, height - cell.y - 1};
  }
}

void Tetromino::flipOnV() {
  for (auto &cell : cells) {
    cell = {width - cell.x - 1, cell.y};
  }
}

Positions& Tetromino::getCells() {
  return cells;
}

const Positions& Tetromino::getCells() const {
  return cells;
}

int Tetromino::getWidth() const {
  return width;
}

int Tetromino::getHeight() const {
  return height;
}

bool Tetromino::contains(Position p) const {
  for (auto c : getCells()) {
    if (p == c)
      return true;
  }
  return false;
}

bool overlap(const Tetromino &x, const Tetromino &y) {
  for (auto p : x.getCells()) {
    if (y.contains(p))
      return true;
  }
  return false;
}

Tetromino& operator+=(Tetromino &t, Position p) {
  for (auto &cell : t.getCells()) {
    cell += p;
  }
  return t;
}

Tetromino& operator-=(Tetromino &t, Position p) {
  for (auto &cell : t.getCells()) {
    cell -= p;
  }
  return t;
}

Position origin(const Tetromino &t) {
  int minX = -1;
  int minY = -1;
  for (auto cell : t.getCells()) {
    if (cell.x < minX || minX == -1)
      minX = cell.x;
    if (cell.y < minY || minY == -1)
      minY = cell.y;
  }
  return {minX, minY};
}

Tetromino Tetromino::fromString(std::string s) {
  Tetromino t;
  int x = 0, y = 0;

  for (auto c : s) {
    switch (c) {
      case '\n': t.width = x > t.width ? x : t.width;
                 x = 0; ++y;
                 break;
      case '0' : t.cells.push_back(Position{x, y});
                 t.height = y;
      default  : ++x;
    }
  }

  ++t.height;

  return t;
}

void Tetromino::renderAt(sf::Vector2i p, int s, sf::RenderWindow &w) const {
  for (auto &cell : getCells()) {
    cell.renderAt(p, s, w, color);
  }
}
