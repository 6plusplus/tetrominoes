#include "Board.hpp"

bool Board::tryAdd(Tetromino newT, Position p) {
  newT += p;

  for (auto tP : newT.getCells()) {
    bool fits = false;
    for (auto cell : cells) {
      fits |= tP == cell;
    }
    if (!fits) return false;
  }

  for (auto &t : tetrominoes) {
    if (overlap(t, newT)) return false;
  }

  tetrominoes.push_back(newT);
  return true;
}

bool Board::tryTake(Tetromino &t, Position &p) {
  auto iter = tetrominoes.begin();
  auto end = tetrominoes.end();
  while (iter != end) {
    if (iter->contains(p)) {
      t = *iter;
      p = origin(t);
      t -= p;
      tetrominoes.erase(iter);
      return true;
    }
    ++iter;
  }
  return false;
}

bool contains(const Tetrominoes& ts, Position p) {
  for (auto &t : ts) {
    if (t.contains(p))
      return true;
  }
  return false;
}

bool Board::isFinished() const {
  for (auto cell : cells) {
    if (!contains(tetrominoes, cell))
      return false;
  }
  return true;
}

Board Board::fromString(std::string s) {
  Board b;
  int x = 0, y = 0;

  for (auto c : s) {
    switch (c) {
      case '\n': x = 0; ++y;
                 break;
      case  '0': b.cells.push_back(Position{x, y});
      default  : ++x;
    }
  }

  return b;
}

void Board::renderAt(sf::Vector2i p, int s, sf::RenderWindow &w) const {
  for (auto &cell : cells) {
    cell.renderAt(p, s, w, sf::Color(127, 127, 127));
  }

  for (auto &t : tetrominoes) {
    t.renderAt(p, s, w);
  }
}

void Board::onFinish() {
  for (auto &t : tetrominoes) {
    t.color = sf::Color(0, 128, 0);
  }
}

int Board::getWidth() const {
  int width = 0;
  for (auto &cell : cells) {
    if (cell.x > width)
      width = cell.x;
  }
  return width;
}

int Board::getHeight() const {
  int height = 0;
  for (auto &cell : cells) {
    if (cell.y > height)
      height = cell.y;
  }
  return height;
}
