#include "Position.hpp"

void Position::renderAt(sf::Vector2i p, int s, sf::RenderWindow &w, sf::Color c) const {
  sf::RectangleShape r(sf::Vector2f(s, s));
  r.setPosition(p.x+x*s, p.y+y*s);
  r.setFillColor(c);
  r.setOutlineColor(sf::Color(192, 192, 192));
  r.setOutlineThickness(-1);
  w.draw(r);
}

Position& operator+=(Position &p, const Position &q) {
  p.x += q.x;
  p.y += q.y;
  return p;
}

Position& operator-=(Position &p, const Position &q) {
  p.x -= q.x;
  p.y -= q.y;
  return p;
}

Position& operator*=(Position& p, int k) {
  p.x *= k;
  p.y *= k;
  return p;
}

Position operator+(Position p, const Position &q) {
  p += q;
  return p;
}

Position operator-(Position p, const Position &q) {
  p -= q;
  return p;
}

Position operator*(Position p, int k) {
  p *= k;
  return p;
}

bool operator==(const Position &p, const Position &q) {
  return p.x == q.x
      && p.y == q.y;
}

bool operator!=(const Position &p, const Position &q) {
  return !(p == q);
}
