#ifndef INC_FreeTetrominoes
#define INC_FreeTetrominoes

#include "Tetromino.hpp"
#include "Renderable.hpp"
#include <list>

struct Foo {
  sf::Vector2i pos;
  Tetromino tet;
};

using MaybeTet = std::list<Foo>::iterator;

class FreeTetrominoes {
  std::list<Foo> tetrominoes;
  int size;
public:
  bool hasTet(const MaybeTet&) const;

  FreeTetrominoes(int);

  MaybeTet put(Tetromino, sf::Vector2i);
  MaybeTet get(sf::Vector2i);
  void remove(MaybeTet);

  MaybeTet empty();

  void render(sf::RenderWindow&) const;
};

#endif // INC_FreeTetrominoes
