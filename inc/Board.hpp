#ifndef INC_Board
#define INC_Board

#include "Tetromino.hpp"
#include "Renderable.hpp"
#include <string>
#include <iostream>

using Tetrominoes = std::vector<Tetromino>;

class Board {
  Positions cells;
  Tetrominoes tetrominoes;
public:
  bool tryAdd(Tetromino, Position);
  bool tryTake(Tetromino&, Position&);
  bool isFinished() const;
  static Board fromString(std::string);
  void renderAt(sf::Vector2i, int, sf::RenderWindow&) const;
  void onFinish();
  int getWidth() const;
  int getHeight() const;
};

#endif // INC_Board
