#ifndef INC_Position
#define INC_Position

#include "Renderable.hpp"

struct Position {
  int x, y;
  void renderAt(sf::Vector2i, int, sf::RenderWindow&, sf::Color) const;
};

Position& operator+=(Position&, const Position&);
Position& operator-=(Position&, const Position&);
Position& operator*=(Position&, int);
Position operator+(Position, const Position&);
Position operator-(Position, const Position&);
Position operator*(Position, int);
bool operator==(const Position&, const Position&);
bool operator!=(const Position&, const Position&);

#endif // INC_Position
