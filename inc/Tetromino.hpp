#ifndef INC_Tetromino
#define INC_Tetromino

#include "Position.hpp"
#include <vector>
#include <string>

using Positions = std::vector<Position>;

class Tetromino {
  int width, height;
  Positions cells;
public:
  sf::Color color = sf::Color(255, 127, 00);
  
  void rotateC();
  void rotateAC();
  void flipOnH();
  void flipOnV();

  Positions& getCells();
  const Positions& getCells() const;

  int getWidth() const;
  int getHeight() const;

  bool contains(Position) const;

  static Tetromino fromString(std::string);

  void renderAt(sf::Vector2i, int, sf::RenderWindow&) const;
};

bool overlap(const Tetromino&, const Tetromino&);
Tetromino& operator+=(Tetromino&, Position);
Tetromino& operator-=(Tetromino&, Position);
Position origin(const Tetromino&);

#endif // INC_Tetromino
