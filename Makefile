flags = -Wall -Werror -pedantic -o3 -std=c++14
incPath = -I inc/
compile = g++ $(flags) $(incPath)

slink =
dlink = -lsfml-window -lsfml-graphics -lsfml-system
link = $(slink) -Bdynamic $(dlink)

headers = $(shell find inc -type f -name "*.hpp")
sources = $(shell find src -type f -name "*.cpp")

main: $(headers) $(sources)
	@mkdir -p bin
	@$(compile) $(sources) -o bin/main $(link)
